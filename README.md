# SimpleCRUD
Simple CRUD application

## Como executar
### 1. Backend
**REST API (.Net Core 2.2)**

Na raiz do código fonte execute:
```
cd Backend
dotnet restore
cd Crud.Infrastructure
dotnet ef database update
cd ..\Crud.API
dotnet run
```

### 2. Frontend
**SPA (Angular 7)**

Na raiz do código fonte execute:
```
cd Frontend\WebSPA
npm install
ng serve --open
```